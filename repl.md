# Usage with REPL

Run evcxr

## Load dependencies

```
:dep .
:dep regex-automata
```

## Use symbols

```
use wordcut_engine::load_dict;
use wordcut_engine::Wordcut;
use wordcut::load_cluster_rules;
use regex_automata::dense::DenseDFA;
use regex_automata::Regex;
use wordcut_engine::load_split_rules;
```

## Initialize Wordcut

```
let dict: Dict = load_dict("data/words_th.txt").unwrap();
let cluster_rules: DenseDFA<Vec<usize>, usize> = load_cluster_rules(Path::new("data/thai_cluster_rules.txt")).unwrap();
let split_rules: Regex = load_split_rules(Path::new("data/thai_split_rules.txt")).unwrap();
let wc: Wordcut = Wordcut::new_with_cluster_re(dict, cluster_rules);
```

## Use

```
println!("{}\n", wc.put_delimiters("ภูมิประเทศ.", "|"));
```
